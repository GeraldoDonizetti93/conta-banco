import java.util.Scanner;

public class Program {
  //TODO: Conhecer e importar aclasse Scanner

  //Exibier as mensagens para o nosso usúario
  //Obter pela lib scanner os valores digitados no terminal
  //Exibier a mensagem de conta criada

  public static void main(String[] args) {
    var scanner = new Scanner(System.in);

    System.out.println("Digite o número da sua agência:");
    var number = Long.valueOf(scanner.next());
    System.out.println("Digite o nome da sua agéncia:");
    var agency = scanner.next();
    System.out.println("Digite seu nome:");
    var name = scanner.next();
    System.out.println("Digite seu saldo em conta:");
    var balance = Double.valueOf(scanner.next());
    var client = new BankAccount(number, agency, name, balance);

    System.out.println("Olá " + client.getClientName() + ", obrigado por criar uma conta em nosso banco sua agência é "
        + client.getAgency() + ", conta " + client.getNumber() + " e seu saldo " + client.getBalance() +
        " já está disponível");
  }
}
