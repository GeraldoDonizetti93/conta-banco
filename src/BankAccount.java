public class BankAccount {
  private Long number;
  private String agency;
  private String clientName;
  private Double balance;

  public BankAccount(Long number, String agency, String clientName, Double balance) {
    this.number = number;
    this.agency = agency;
    this.clientName = clientName;
    this.balance = balance;
  }

  public Long getNumber() {
    return number;
  }

  public String getAgency() {
    return agency;
  }

  public String getClientName() {
    return clientName;
  }

  public Double getBalance() {
    return balance;
  }
}
